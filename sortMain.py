import random
import  time
import matplotlib.pyplot as plt

from sort_algo import mergeSort, insertionSort

mergeSort_time = []
insertionSort_time = []
item_array = []
item = 1000

if __name__=="__main__":
    for i in range(10):
        #merge sort

        data = random.sample(range(1000000000), item)
        start = time.time()
        mergeSort(data, 0, len(data) - 1)
        end = time.time()
        mergeSort_time.append(end - start)

        #insertion sort
        data = random.sample(range(1000000000), item)
        start = time.time()
        insertionSort(data)
        end = time.time()
        insertionSort_time.append(end - start)

        item_array.append(item)
        item += 2000

plt.plot(item_array, mergeSort_time, label="Merge Sort")
plt.plot(item_array, insertionSort_time, label="Insertion Sort")
plt.xlabel("Items")
plt.ylabel("Execution time(ms)")
plt.title("Merge Sort VS Insertion Sort")
plt.legend()
plt.show()
