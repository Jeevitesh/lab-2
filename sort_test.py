import unittest
from sort_algo import mergeSort, insertionSort

class TestSort(unittest.TestCase):
    def testSort_merge(self):
        data = [10, 6, 89, 21, 69, 12, 34, 61]
        answer = [6, 10, 12, 21, 34, 61, 69, 89]
        mergeSort(data, 0, len(data) - 1)
        self.assertListEqual(data, answer)

    def testSort_insertion(self):
        data = [10, 6, 89, 21, 69, 12, 34, 61]
        answer = [6, 10, 12, 21, 34, 61, 69, 89]
        insertionSort(data)
        self.assertListEqual(data, answer)

if __name__ == "__main__":
    unittest.main()